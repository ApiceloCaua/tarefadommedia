// Criação do array para armazenar as notas
let notas = [];
let contadorNotas = 1;

// Função para adicionar a nota ao array e limpar o campo de entrada
document.getElementById("add_nota").addEventListener("click", function() {
    const notaInput = document.getElementById("nota");
    const nota = parseFloat(notaInput.value);

    if (isNaN(nota)) {
        alert("A nota digitada é inválida, por favor, insira uma nota válida.");
        return;
    }

    if (nota < 0 || nota > 10) {
        alert("Digite uma nota válida entre 0 e 10.");
        return;
    }

    notas.push(nota);
    notaInput.value = "";
    atualizarNotasAdicionadas();
});

// Função para calcular a média e exibi-la
document.getElementById("cal_md").addEventListener("click", function() {
    if (notas.length === 0) {
        alert("Adicione notas antes de calcular a média.");
        return;
    }

    const somaNotas = notas.reduce((acc, nota) => acc + nota, 0);
    const media = somaNotas / notas.length;
    const mediaElement = document.getElementById("media");
    mediaElement.textContent = `A média é: ${media.toFixed(2)}`;
});

// Função para atualizar a lista de notas adicionadas
function atualizarNotasAdicionadas() {
    const boxMessage = document.querySelector(".box_message");
    boxMessage.innerHTML = "<p>Notas adicionadas:</p>";
    notas.forEach((nota, index) => {
        const notaElement = document.createElement("p");
        notaElement.textContent = `Nota ${index + 1}: ${nota.toFixed(2)}`;
        boxMessage.appendChild(notaElement);
    });
}
